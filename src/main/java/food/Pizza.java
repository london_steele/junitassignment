package food;
import bearman.Bearman;
import bearman.PizzaEatingBearman;
//import bearman.PizzaEatingBearman;

public class Pizza extends Food {

	/**
	 * Pizza satisfies the pizza eating bearman
	 * 
	 * @param pizzaEater
	 *            The PizzaEatingBearman to feed
	 */
	@Override
	public void feedBearman(PizzaEatingBearman pizzaEater) {
		pizzaEater.setHungry(false);
	}
}
